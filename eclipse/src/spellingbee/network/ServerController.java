package spellingbee.network;

import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	//private ISpellingBeeGame spellingBee = new SimpleSpellingBeeGame();
	private static final String myPath="src\\datafiles\\scores.txt";
	private static HighScores myHighScores=new HighScores(myPath);
	//stores the path to the file as a final String

	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 * @author Cathy Tham
	 * @author Maria Barba
	 * @author C�line Tran
	 */
	public String action(String inputLine) {
		if(inputLine.equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}

		//Client: "wordCheck;[word]"
		String[] request=inputLine.split(";");
		if(request[0].equals("wordCheck")) {
			
			String points=Integer.toString(spellingBee.getPointsForWord(request[1]));
			String message=spellingBee.getMessage(request[1]);
			//"good:[points]" or "not a word:0"
			return message+" Points: "+points;
		}

		if(inputLine.equals("getCenterLetter")) {
			return Character.toString(spellingBee.getCenterLetter());
		}

		if(inputLine.equals("getTotalPoints")) {
			return Integer.toString(spellingBee.getScore());
		}

		if(inputLine.equals("getBrackets")) {
			int[] brackets=spellingBee.getBrackets();
			return brackets[0]+":"+brackets[1]+":"+brackets[2]+":"+brackets[3]+":"+brackets[4];	
		}
		
		if(request[0].equals("addScore")) {
			int userScore= spellingBee.getScore();
			System.out.println("This is the user score"+userScore);
			//String[] providedVals=inputLine.split(";");
			String userName=request[1];
			myHighScores.addResult(userName, userScore);
			//will save the new Score into the arrayList of previous games inside the HighScores class
			myHighScores.saveResult(myPath);
			//will actually save the updated array list of score called previous games into the scores.txt file (inside datafiles folder)
		return "Updated file";
		}
		if(inputLine.equals("getTopTen")) {
			if(myHighScores.getPreviousGames().size()<10) {
				return "Not enough scores!";
			//If the number of scores is less than 10, return the message 
					}
			else {
			String returnedTopTen=myHighScores.getTopTen();
			//This will return the top 10 games in a string format 
			//:name;score:name;score:name;score:name;score:name;score:name;score:name;score:name;score:name;score:name;score
			return returnedTopTen;
		}

	}
		return null;
}
}

