package spellingbee.client;

import spellingbee.network.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.text.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * This class extends Tab and displays how the user is progressing through the brackets.
 * @author Cathy Tham
 */

public class ScoreTab extends Tab {
	private Client client;
	private String[] brackets;
	
	//create text label on the left side of the grid 
	private Text queenBee;
	private Text genius;
	private Text amazing;
	private Text good;
	private Text gettingStarted;
	private Text currentScore;
	//create text label on the right side of the grid 
	private Text currentScoreScore;
	private Text queenBeeScore;
	private Text geniusScore;
	private Text amazingScore;
	private Text goodScore;
	private Text gettingStartedScore;


	public ScoreTab(Client client) {
		super("Score");
		this.client=client;
		brackets=getBrackets();
		GridPane pane = createGridPane();
		this.setContent(pane);
	}
	
	/**
	 * This method creates the layout of the GUI of the ScoreTab
	 * @return GridPane that represent the layout of the ScoreTab
	 */
	private GridPane createGridPane() {
		GridPane pane=new GridPane();
		//create text label on the left side of the grid 
		queenBee=new Text("Queen Bee");
		genius=new Text("Genius");
		amazing=new Text("Amazing");
		good=new Text("Good");
		gettingStarted=new Text("Getting Started");
		currentScore=new Text("Current Score");
		
		//add text obj
		pane.add(queenBee, 0, 0);
		pane.add(genius, 0, 1);
		pane.add(amazing, 0, 2);
		pane.add(good, 0, 3);
		pane.add(gettingStarted, 0, 4);	
		pane.add(currentScore, 0, 5);
		
		//style text obj
		queenBee.setFill(Color.GREY);
		genius.setFill(Color.GREY);
		amazing.setFill(Color.GREY);
		good.setFill(Color.GREY);
		gettingStarted.setFill(Color.GREY);
		pane.setHgap(10);
		pane.setVgap(2);
		
		//create text label on the right side of the grid
		queenBeeScore = new Text(brackets[4]);
		geniusScore= new Text(brackets[3]);
		amazingScore = new Text(brackets[2]);
		goodScore = new Text(brackets[1]);
		gettingStartedScore = new Text(brackets[0]);
		currentScoreScore = new Text("0");
		//style currentScoreScore text obj
		currentScoreScore.setFill(Color.RED);
		//add score text obj
		pane.add(queenBeeScore, 1, 0);
		pane.add(geniusScore, 1, 1);
		pane.add(amazingScore, 1, 2);
		pane.add(goodScore, 1, 3);
		pane.add(gettingStartedScore, 1, 4);	
		pane.add(currentScoreScore, 1, 5);
		
		return pane;
	}
	
	/**
	 * This method send a message to the server which returns the brackets
	 * @return String[] that contains the brackets number(points)
	 */
	private String[] getBrackets() {
		String response=client.sendAndWaitMessage("getBrackets");
		brackets = response.split(":");
		return brackets;
	}
	
	/**
	 * This method makes a call to the server to get the current score and update the GUI
	 */
	public void refresh() {
		String score = client.sendAndWaitMessage("getTotalPoints");
		currentScoreScore.setText(score);
		int currentScore=Integer.parseInt(score);

		int[] bracketsToInt = bracketsToInt();
		styleCategory(bracketsToInt, currentScore);
	 
	}
	
	/**
	 * This method will check if the score is higher or equal than a bracket's number and set to the correct color (gray or black)
	 * @param brackets int[] that represent the bracket's numbers 
	 * @param currentScore int that represent the currentScore of the user
	 */
	private void styleCategory(int[] bracketsToInt, int currentScore) {
		//if score if bigger than bracket's numbers
		if (currentScore >= bracketsToInt[4]) {
			queenBee.setFill(Color.BLACK);
		}
		if (currentScore >= bracketsToInt[3]) {
			genius.setFill(Color.BLACK);
		}
		if (currentScore >= bracketsToInt[2]) {
			amazing.setFill(Color.BLACK);
		}
		if (currentScore >= bracketsToInt[1]) {
			good.setFill(Color.BLACK);
		}
		if (currentScore >= bracketsToInt[0]) {
			gettingStarted.setFill(Color.BLACK);
		}
		
	}


	/**
	 * This method converts the brackets (String) to an integer
	 * @return integer array of the bracket's numbers
	 */
	private int[] bracketsToInt() {
		int[] bracketsToInt = new int[this.brackets.length];
		for(int i = 0 ; i < brackets.length ; i++) {
			bracketsToInt[i] = Integer.parseInt(brackets[i]);
		}
		return bracketsToInt;
	}

}
