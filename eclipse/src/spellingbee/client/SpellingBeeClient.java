package spellingbee.client;

import spellingbee.network.*;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;

/**
 * This class is the main place where the GUI will start to run the whole program. 
 * @Author Celine Tran  
 * @Author Maria Barba  
 * @Author Cathy Tham  
 * @version 12-2020
 */
public class SpellingBeeClient extends Application {
	/**
	 * The client object.
	 */
    private Client client = new Client();

    /**
     * This method will start the GUI application with the gameTab, scoreTab and highScoreTab.
     * @param stage The stage.
     */
    public void start(Stage stage) {
        Group root = new Group();
        //create scene and associate to root
        Scene scene = new Scene(root, 450, 300);
        TabPane tabpane = new TabPane();
        GameTab gametab = new GameTab(client);
        ScoreTab scoreTab = new ScoreTab(client);
        HighScoresTab highScoresTab = new HighScoresTab(client);
        tabpane.getTabs().addAll(gametab, scoreTab, highScoresTab);
        root.getChildren().add(tabpane);
        /**
         * Event listener that detects when there is any change to the score field.
         * When there is a change, it calls the refresh method from the ScoreTab.
         * @Cathy Tham
         */
        TextField score = gametab.getScoreField();
        score.textProperty().addListener(
            new ChangeListener < String > () {
                public void changed(ObservableValue < ? extends String > observable, String old, String newV) {
                    scoreTab.refresh();
                }
            });
        //associate scene to stage and show
        scene.setFill(Color.YELLOW);
        stage.setTitle("Spelling Bee Game");
        stage.setScene(scene);
        stage.show();

    }

    /**
     * This is the main method where we will call the launch to start the GUI.
     */
    public static void main(String[] args) {
        Application.launch(args);
    }

}