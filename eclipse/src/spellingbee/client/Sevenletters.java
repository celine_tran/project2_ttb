package spellingbee.client;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import spellingbee.network.Client;
import java.util.ArrayList;
import java.util.List;

/**
 * This class will create and initialize the seven letter buttons for the GameTab GUI.
 * @Author Celine Tran 
 *  @version 12-2020
 */
public class Sevenletters extends HBox {
	 /**
	  * The client object
	  */
    private Client client;

    /**
     * This constructor will associate each of the 7 letters to buttons and emphasize the centerLetter in red.
     * It will also have a handler for the button being pressed and added to text.
     * @param input The word that the user typed in the GUI.
     * @param client The client object from the client network 
     */
    public Sevenletters(TextField input, Client client) {
        this.client = client;
        List < Button > buttonArr = new ArrayList < Button > ();
        List < Character > letterArr = new ArrayList < Character > ();
        String letters = this.client.sendAndWaitMessage("getAllLetters");
        String center = this.client.sendAndWaitMessage("getCenterLetter");
        
        //adds 7 buttons and letters to the arrays created above. 
        //Style the center letter and set handler on the buttons for click event.
        for (int i = 0; i < letters.length(); i++) {
            if (i == findCenterPosition(letters, center)) {
                letterArr.add(center.charAt(0));
                buttonArr.add(new Button("" + letterArr.get(i) + ""));
                buttonArr.get(i).setStyle("-fx-text-fill: #ff0000");
                
            } else {
                letterArr.add(letters.charAt(i));
                buttonArr.add(new Button("" + letterArr.get(i) + ""));
            }
            
            buttonArr.get(i).setOnAction(new ButtonLettersHandler(letterArr.get(i), input));     
            this.getChildren().addAll(buttonArr.get(i));
        }
    }

    /**
     * This method will find the position of the center letter.
     * @param letters The letters passed in from the constructor.
     * @param center The center letter from the getCenterLetter method in the server.
     *  @return The position of the center letter.
     */
    public int findCenterPosition(String letters, String center) {
        int i = 0;
        for (; i < letters.length(); i++) {
            if (letters.charAt(i) == center.charAt(0)) {
                return i;
            }
        }
        return i;
    }
}