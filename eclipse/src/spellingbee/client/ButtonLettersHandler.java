package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * This class handles the button click event from the SevenLetters class.
 * @Author Celine Tran 
 *  @version 12-2020
 */
public class ButtonLettersHandler implements EventHandler<ActionEvent>  {
	 /**
	  * The text field where the input will be put.
	  */
	private TextField input;
	
	 /**
	  * The letter contained in the 7 letters used for the game.
	  */
	private char letter;
	
	/**
	 * This constructor initializes the ButtonLetterHandler object.
	 * @param letter The letter from the String stored by the spelling bee object.
	 * @param input The letter seen on the button.
	 */
	public ButtonLettersHandler(char letter,TextField input) {
		super();
		this.input = input; 
		this.letter=letter;
	}
	
	/**
	 * This class handles the button click event. It takes the letter seen on the button and puts it in the text.
	 * @param e The event passed in by the letter buttons being pressed.
	 */
	@Override
	public void handle(ActionEvent e) {
		input.setText(input.getText()+String.valueOf(letter));
	}
}

