package spellingbee.client;

import spellingbee.network.*;
import javafx.scene.control.Tab;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import spellingbee.network.Client;
import spellingbee.server.HighScores;

/**
 * This class will display the top 10 results to the user.It will contain a
 * textfield for the user's name,a button for the user to submit their name and
 * score to the server object. A panel will display the best 10 results of
 * previous runs and a refresh button will be used to query the server to get an
 * updated top 10 list and display the results in the client.
 * 
 * @author Maria Barba @ version 12-09
 */
public class HighScoresTab extends Tab {
	private Client client;
	// use a private Client
	private HighScores myHighScores = new HighScores();

	// create a private HighScores object
	public HighScoresTab(Client client) {
		super("High Scores");
		this.client = client;
		// set the client with the provided client

		HBox hbox = new HBox();
		// hbox will store user name input field,the save and the refresh button
		HBox toptenHbox = new HBox();
		// toptenHbox will store the refreshed values of the top 10 Games
		VBox overallScoresTab = new VBox();
		// create a new VBox object
		Button save = new Button("Save Score");
		// a save button to save the user score
		Button refresh = new Button("Refresh");
		// refresh button will refresh the top 10 scores
		TextField userData = new TextField("Enter User Name ");
		// userData is where the user will provide the user name
		TextField topTenScores = new TextField();
		// will store a text area with the top 10 scores that the user can refresh
		topTenScores.setPrefSize(450, 100);
		hbox.getChildren().addAll(save, userData, refresh);
		toptenHbox.getChildren().add(topTenScores);
		overallScoresTab.getChildren().addAll(hbox, toptenHbox);
		// add all the elements to overallScoresTab
		setContent(overallScoresTab);
		// set the content of the tab with the hbox.
		refresh.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				String response = client.sendAndWaitMessage("getTopTen");
				// make a request to the server to get the top ten results
				String[] pieces = response.split(":");
				// split the response into an array of strings
				String results = "";
				// create a String for the return response
				for (int i = 0; i < pieces.length; i++) {
					results = results + pieces[i] + "|-->|";
					// put every name in |name;score|---> format
				}
				topTenScores.setText(results);
				// will set the text field to have the top ten score or a message
				// if we have less than 10 scores
			}
		});
		save.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				String providedData = userData.getText();
				// will get the user name from the textFiled
				String[] onlyName = providedData.split("\\s");
				// Will only get the first element written by the user
				String response = client.sendAndWaitMessage("addScore;" + onlyName[0]);
				// Will retrieve the response from the server after we pass the name of userName
				// to add it to a result
			}

		});
		// the save button will take the user input and will save it into the file
		// containing the user scores

	}
}
