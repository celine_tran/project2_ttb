package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import spellingbee.network.Client;

/**
 * This class one of the three tab for the spelling bee game. It is where the user will enter words the words for the game. 
 * @Author Celine Tran  
 * @version 12-2020
 */
public class GameTab extends Tab {
	 /**
	  * The client object.
	  */
    private Client client;
    
    /**
	  * A text field for the user score.
	  */
    private TextField score;
    
    /**
	 * A constructor that creates the element inside the tab pane.
	 * @param client The client object from the client network.
	 */
    public GameTab(Client client) {
        super("Game");
        this.client = client;
        VBox vbox = new VBox();
        TextField input = new TextField();
        Sevenletters svL = new Sevenletters(input, this.client);
        Button submit = new Button("Submit");
        HBox hbox = new HBox();
        TextField msg = new TextField();
        msg.setPrefWidth(300.0);
        score =  new TextField();
        
        //styling
        vbox.setSpacing(10);
        input.setStyle("-fx-control-inner-background: black;");
        msg.setStyle("-fx-control-inner-background: black;");
        score.setStyle("-fx-control-inner-background: black;");
        
        //An anonymous event handler for the submit button. It will set the msg and score textField by retrieving from the server.
        submit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				String message = client.sendAndWaitMessage("wordCheck;"+input.getText());
				msg.setText(message);
				String scoreRes= client.sendAndWaitMessage("getTotalPoints");
				score.setText(scoreRes);
			}
			});
        hbox.getChildren().addAll(msg, score);
        vbox.getChildren().addAll(svL, input, submit, hbox);
        setContent(vbox);
    }

    /**
   	 * This method returns the current total score from the score TextField.
   	 * @return The score from the input. 
   	 */
    public TextField getScoreField() {
        return score;
    }
}