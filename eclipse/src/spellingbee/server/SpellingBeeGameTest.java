package spellingbee.server;

import spellingbee.network.*;

import java.io.IOException;

/**
 * This class is used to test the SpellingBeeGame get methods.
 * @author Cathy Tham
 */
public class SpellingBeeGameTest {
	public static void main(String[] args) throws IOException {
		//String source = "C:\\cathy\\courses\\java310\\project2_ttb\\eclipse\\src\\datafiles\\english.txt";
		

		/*SpellingBeeGame game=new SpellingBeeGame("ertyhsd");

		int[] brackets = game.getBrackets();
		
		System.out.println();
		for(int i : brackets) {
			System.out.println(i);
		}
		
		System.out.println("seven letters "+game. getSevenLetters());
		System.out.println(game.getAllLetters());
		System.out.println(game.getCenterLetter());
		
		SpellingBeeGame game2=new SpellingBeeGame();
		int[] brackets2 = game2.getBrackets();
		System.out.println();
		for(int i : brackets) {
			System.out.println(i);
		}
		System.out.println("seven letters "+game2. getSevenLetters());
		System.out.println(game2.getAllLetters());
		System.out.println(game2.getRandomCenterLetter());*/
		
		//System.out.println(game.createWordsFromFile(source ));
		
		ServerController test = new ServerController();
		System.out.println("Test");
		System.out.println(test.action("getAllLetters"));
		System.out.println(test.action("getCenterLetter"));
		System.out.println(test.action("getTotalPoints"));
		System.out.println(test.action("getBrackets"));
		String[] brackets=getBrackets(test.action("getBrackets"));
		System.out.println(brackets[4]);
		System.out.println();
		for(String i : brackets) {
			System.out.println(i);
		}
		System.out.println(test.action("wordCheck;hello"));
		System.out.println("---------------");
		}
	
	public static String[] getBrackets(String brackets) {
		String[] bracket = brackets.split(":");
		return bracket;
	}

}
