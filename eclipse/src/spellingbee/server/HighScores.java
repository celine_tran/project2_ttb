package spellingbee.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.io.*;
import java.nio.file.*;
/**
 * This class will store a list of the user's scores in a private arraylist
 * which all spelling bee games are stored in sorted order based.
 * @author Maria Barba
 * @version 12-08
 */
public class HighScores  {
	private ArrayList<Score> previousGames;
	/**
	 * The constructor takes nothing as input and will initialize scores to be an empty array list.
	 */
	public HighScores() {
		this.previousGames = new ArrayList<Score>(0);
	}
	/**
	 * This constructor takes as input a String representing the path to the file containing the user name 
	 * and the points of each user.It will initialize the scores array list based on values from the file, it
	 * should also sort that array list in decreasing order .However if the file is missing or not found,it will 
	 * initialize the array list to be empty.
	 * @param String path
	 */
	public HighScores(String path) {
		Path pObj = Paths.get(path);
		//creates an object of type path with the provided path
		List<String> scoresData = null;
		//creates an array list of string
		try {
		scoresData = Files.readAllLines(pObj);
		//reads all lines from the path
		} catch (IOException e) {
		System.out.println("Oops you have an error !");
		}
		finally {
		this.previousGames = new ArrayList<Score>();	
		//whether the file is found or not, it will initialize an array list to be empty
		
		}
		
		for (String s : scoresData) {
			//will loop through the array list of string
			String[] resultArr=s.split(";");
			//split data from each line on the ";" character
			Score oneScore=new Score();
			//create a new Score object
			for(String m : resultArr) {
				if(!checkIfInt(m)) {
					//will loop through the array of strings and will
					//check if the data is a score or a user name,if it is
					//a user name then we set the Score object ,user name .
					oneScore.setUserName(m);
				}
				else {
					oneScore.setUserScore(Integer.parseInt(m));
					//will set the score of the user on the Score object after transforming the score to an integer
				}
			
			}
			this.previousGames.add(oneScore);
			//will add the Score object to the previousGames array list
			Collections.sort(this.previousGames);
			//will sort the previousGames array in decreasing order	
		}
		  
	}
	/**
	 * The getter will return the array list of the player's scores for previous games
	 * @return the previousGames ArrayList<Score>
	 */
	public ArrayList<Score> getPreviousGames() {
		return previousGames;
	}
	/**
	 * The setter will set the array list of previous games 
	 * @param previousGames 
	 */
	public void setPreviousGames(ArrayList<Score> previousGames) {
		this.previousGames = previousGames;
	}
	/**
	 * This method takes as input a String and will check 
	 * if the string can be transformed to an integer,will return true 
	 * if it can be transformed to an integer otherwise will return false.
	 * @param scoreValue String
	 * @return a boolean depending if the input can be transformed to an integer
	 */
	public boolean checkIfInt(String scoreValue) {
	try {	
		Integer.parseInt(scoreValue);
		
	}
	catch (NumberFormatException e ) {
		return false;
	}
	    return true;
	}
	/**
	 * AddResult takes as input the user name and score.It creates a Score
	 * object and inserts it in the appropriate spot in the array list so that
	 * the array list remains sorted.
	 * @param userName String
	 * @param score int
	 */
	public void addResult(String userName, int score) {
	Score newScoreVal=new Score();
	//creates a new Score object 
	newScoreVal.setUserName(userName);
	//sets the Score object with the user name
	newScoreVal.setUserScore(score);
	//sets the Score object with the score
	ArrayList<Score> newAddedVals= new ArrayList<Score>();
	//creates an array list of Score 
	int addToIndex=0;
	//will keep the index at which to insert the score object in previousGames
	for(Score myscore : this.previousGames) {
		if(!(myscore.getUserScore()>score)) {
		//look for the index at which the score we are
		//trying to insert is
			addToIndex=this.previousGames.indexOf(myscore);
			//store the index at which the score is more than the score we are trying to insert
			
			break;
			//as soon as we found the appropriate index,we exit the loop
		}
	}
	this.previousGames.add(addToIndex, newScoreVal);	
	//add the score object at the appropriate position inside the array list
	}
	/**
	 * This method exports the results to a file of the same format as
	 * the input
	 * @param somefilepath String
	 */
	public void saveResult(String somefilepath) {
	Path filepath=Paths.get(somefilepath);
	//create a Path object from the provided path
	List<String> previousGamesInSting = new ArrayList<String>();
	//create an array list of strings
	for(Score s: this.previousGames) { 
		previousGamesInSting.add(s.toString());
		//transform all values from previousGames into strings and add them to an array list of strings
	}
	try {
	Files.write(filepath, previousGamesInSting);
	//write the values from the array list of string into the file
	}
	catch(IOException e) {
	System.out.println("file not found");	
	}
	}
	/**
	 * This method will return a String containing all the necessary information
	 * to display the best 10 user results
	 * @return String
	 */
	public String getTopTen() {
	String topTenScores="";
	if(previousGames.size()<0) {
		return "Not enough scores, please have 10";
	}
	else {
	for(int i=0; i<10;i++) {
		topTenScores=topTenScores+":"+this.previousGames.get(i).toString();
	//will create a string containing 10 top games from previous games.
	}
	
	return topTenScores;
	}
	}
}
