package spellingbee.server;

/**
 * This class is an interface that specifies the required methods that the SpellingBeeGame and SimpleSpellingBeeGame should have.
 *@Author Cathy Tham
 *@author C�line Tran
 *@author Maria Barba
 *@version 12-2020 
 */
public interface ISpellingBeeGame {
	
	/**
	 * This gets the how much the word is worth according to the Spelling Bee rules.
	 */
	int getPointsForWord(String attempt);
	
	/**
	 * This checks the validity of the word and returns a message according to the Spelling Bee rules.
	 */
	String getMessage(String attempt);
	
	/**
	 * Returns 7 letters the the spelling bee object stores.
	 */
	String getAllLetters(); 
	
	/**
	 * Returns the center letter that is required to be part of every word.
	 */
	char getCenterLetter(); 

	/**
	 * Gets the user's current score.
	 */
	int getScore(); 

	/**
	 * Returns the point category the user reaches.
	 */
	int[] getBrackets(); 

}

