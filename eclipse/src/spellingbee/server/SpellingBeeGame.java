package spellingbee.server;

import java.util.HashSet;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.*;
import java.util.*;

/**
 * This class implements the ISpellingBeeGame interface and stores all the information necessary for the Spelling Bee Game.
 * @author Cathy Tham
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	private String letters;
	private char centerLetter;
	private int score;
	//use hashSet because faster to check if an element is inside or not
	private HashSet<String>  wordsFound;
	private static HashSet<String>  possibleWords= createWordsFromFile("src\\datafiles\\english.txt");
	
	
	/**
	 * This constructor chooses a random set of 7 letters from the file and choose randomly the centerLetter
	 */
	
	public SpellingBeeGame() {
			this.letters=getSevenLetters();
			this.centerLetter=getRandomCenterLetter();
			this.score=0;
			this.wordsFound= new HashSet<String>();
	}
	
	/**
	 *This constructor takes as input a String representing the 7 characters instead of randomly generating them.
	 * @param input String represent the seven letters input by the user
	 */
	 
	public SpellingBeeGame(String input) {
			this.letters=input;
			this.centerLetter=getRandomCenterLetter();
			this.score=0;
			this.wordsFound= new HashSet<String>();
	}
	 
	/**
	 * This function take the file from the path and merges all the lines together into a single list and add it to the hasSet
	 * @param path String that represent the path of the english.txt
	 * @return wordsFound HashSet<String> that contains all english words from english.txt
	 */
	private static HashSet<String> createWordsFromFile(String path) {
		//get file path
		try{
			Path pathToFile = Paths.get(path);
			List<String> lines = Files.readAllLines(pathToFile);
			HashSet<String> possibleWords = new HashSet<String>();
			possibleWords.addAll(lines);
			return possibleWords;	
		} 
		
		catch (IOException e) {
			System.out.println("No file found");	
		}
		return null;
	}
	
	
	/**
	 * This method randomly generate a char from the seven letters.
	 * @return char 
	 */
	private char getRandomCenterLetter() {
		Random rand= new Random();
		char randomCenterLetter=letters.charAt(rand.nextInt(letters.length()));
		return randomCenterLetter;
		
	}
	
	/**
	 * This method randomly generate the line that contains the seven letters
	 * @return String that contains the seven letters
	 */
	private String getSevenLetters() {
		try{
			Path  pathToFile = Paths.get("src\\datafiles\\letterCombinations.txt");
			List<String> lines = Files.readAllLines( pathToFile);
		
			//Gets a random line from the file
			Random randgen = new Random();
			letters= lines.get(randgen.nextInt(lines.size()));
			return letters;
		}
		catch (IOException e) {
			System.out.println("No file found");	
		}
		return null;
			
	}

	/**
	 * This method calculate the number of points the player's word and add it the current score if the word exist and it's valid.
	 * @param attempt String that represents the player's word
	 * @return int that represents the points given for the word.
	 */
	@Override
	public int getPointsForWord(String attempt) {
		System.out.print(isWordUsed(attempt));
		if (validateWord(attempt) && languageValidation(attempt) && !(isWordUsed(attempt))) {
			score += calculatePoints(attempt);
			System.out.println(calculatePoints(attempt));
			return calculatePoints(attempt);
		}
		else {
			return 0;
		}
	}

	/**
	 * This method check if the player's word is already guess or not
	 * @param word String that represents the player's word
	 * @return boolean that represent if the player's word is inside the wordsFound HashSet or not
	 */
	private boolean isWordUsed(String word) {
		return wordsFound.contains(word);
	}

	/**
	 * This method returns a message that tell the player if the word is good or not by checking if it's valid and add it to the wordsFound HashSet.
	 * @param attempt String that represents the player's word
	 * @return String that represents the a message depending if the word is good or not
	 */
	@Override
	public String getMessage(String attempt) {
		if (validateWord(attempt) && languageValidation(attempt) && !(isWordUsed(attempt))) {
			wordsFound.add(attempt);
			System.out.println(score);
			return "Good";
		}
		else if (isWordUsed(attempt)) {
			return "Word already used";
			
		}
		else if (attempt.length()<4) {
			return "Word must use more than 4 letters";
		}
		else if (attempt.indexOf(centerLetter)==-1) {
			return "Word doesn't contain the center letter";
		}
		else if (!(languageValidation(attempt))){
			return "Word doesn't exist";
		}
		
		else if(!(validateWord(attempt))) {
			return "Word contains a letter not in the set";
		}

		else {
			return "Error not found";
		}

	}


	@Override
	public String getAllLetters() {
		return letters;
	}

	@Override
	public char getCenterLetter() {
		return centerLetter;
	}


	@Override
	public int getScore() {
		System.out.println(score);
		return score;
	}



	/**
	 * This method calculate the maximum number of points that a player can get using the 7 letter. Takes each word in possibleWords.
	 * @return int[] that represent 5 numbers, the 25% value, the 50% value, the 75%, the 90%, and the 100%.
	 */
	@Override
	public int[] getBrackets() {
		int maxNumberOfPoints = 0;
		//Loops through the words in possibleWords
		for (String word : possibleWords) {
			//check if the word is valid
			if(validateWord(word)) {
				//calculate the point for the word
				maxNumberOfPoints+=calculatePoints(word);
			}
			else {
				maxNumberOfPoints+=0;
			}

		}
		
		int[] brackets=new int[5];
		brackets[0]=(int) (maxNumberOfPoints*0.25);
		brackets[1]=(int) (maxNumberOfPoints*0.50);
		brackets[2]=(int) (maxNumberOfPoints*0.75);
		brackets[3]=(int) (maxNumberOfPoints*0.90);
		brackets[4]=maxNumberOfPoints;
		return brackets;
	}
	
	/**
	 * This method checks if the word contains the middle letter and contains only the letters in the set of seven letters 
	 * @return boolean 
	 */
	private boolean validateWord(String word) {
		//centerLetter not found
		if(word.indexOf(centerLetter)==-1) {
			return false;
		}
		if(word.length()<4) {
			return false;
		}
		int count = 0;
		//used to go through the letters of the word
		for(int i = 0 ; i < word.length() ; i++) {
			//used to go through the seven letters of the set
			for(int j = 0 ; j < 7 ; j++) {
				//make sure the user use the letters in the set only
				if(word.charAt(i) == letters.charAt(j)) {
					count++;
				}
			}
		}
		if(word.length()==count) {
			return true;
		}
		else {
			return false;
		}
	}

	
	/**
	 * This method calculate the number of points given per word depending on the length and if it is a panagram
	 * @param word String 
	 * @return int the number of points per word
	 */
	private int calculatePoints(String word) {
		int points=0;
		int bonus=7;
		//panagram= if use all 7 letters in word, get a bonus 7 points in addition to the amount you got for the words length
		if(checkPanagram(word)) {
			points=word.length()+bonus;	
		}
		else if(word.length()<4) {
			points=0;
		}
		else if(word.length()==4) {
			points=1;
		}
		//word.length()>4 
		else {
			points=word.length();
			
		}
		return points;
		
	}
	/**
	 * This method check if the player's word contains all 7 letters of the set
	 * @param attempt String that represents the player's word
	 * @return boolean that represent if the word is a panagram or not
	 */
	private boolean checkPanagram (String attempt) {
		int count = 0;
		for(int i = 0; i < 7; i++) {
			//check if letters.charAt(i) is inside the word, it is supposed to have 7 counts = 7 letters
			if(attempt.contains(Character.toString(letters.charAt(i)))) {
				count++;
			}
		}
		if(count == 7) {
			return true;
		}
		else {
			return false;
		}
	} 

	
	/**
	 * This method checks if the word is inside the letterCombinaison.txt (dictionary)
	 * @param playerWord String player's word 
	 * @return boolean that represent if the word is in the dictionary or not
	 */
	private boolean languageValidation(String playerWord) {
		boolean isValidWord;
		if (possibleWords.contains(playerWord)) {
					isValidWord = true;
		}
		else {
			isValidWord = false;
		}
		return isValidWord;	
	}

}

