/**
 * This class will create a Score object to be used in 
 * the HighScores class in the arraylist of scores.
 * @author Maria Barba
 * @version 12-08
 */
package spellingbee.server;

import java.util.Arrays;

public class Score implements Comparable<Score> {
	private String userName;
	private int userScore;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userScore
	 */
	public int getUserScore() {
		return userScore;
	}

	/**
	 * @param userScore the userScore to set
	 */
	public void setUserScore(int userScore) {
		this.userScore = userScore;
	}

	public int compareTo(Score score) {
		// will sort Score based on score in decreasing order
		if (this.userScore < score.userScore) {
			return 1;
		}
		if (this.userScore > score.userScore) {
			return -1;
		}
		return 0;
	}

	@Override
	public String toString() {
		return userName + ";" + userScore;
		// will return the string made up of a username;score
	}

}
