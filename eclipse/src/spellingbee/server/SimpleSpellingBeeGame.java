package spellingbee.server;

/**
 * This class is a simplified version of the actual spellingBeeGame class.
 * It allows the implementation of the  ISpellingBeeGame interface more quickly.
 *@Author Celine Tran 
 *@version 12-2020 
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	 /**
	  * An array of String that will be filled with valid words.
	  */
    private String[] valid;
    
    /**
	  * An array of String that will be filled with invalid words.
	  */
    private String[] invalid;
    
    /**
	  * The total accumulated of points.
	  */
    private int pointCount;
    
    /**
	  * The 7 letters used for the game that will be hard coded.
	  */
    private String letters;
    
    /**
	  * The center letter that the user word is required to contain.
	  */
    char center;

    /**
     * This constructor will initialize the valid and invalid array of string with hard coded values. 
     * It will also initialize the pointCount,letters and center letter. 
     */
    public SimpleSpellingBeeGame() {
        valid = new String[4];
        invalid = new String[4];
        this.pointCount = 0;
        this.letters = "ydaamce";
        this.center = 'a';
        
        valid[0] = "academy"; //the panagram
        valid[1] = "aced";
        valid[2] = "made";
        valid[3] = "decay";

        invalid[0] = "cyme";
        invalid[1] = "day";
        invalid[2] = "dye";
        invalid[3] = "demy";
    }

    /**
     *  This method returns points according to the attempt. 
     *  The worth of the points are determined by the spelling bee rules.
     *  @param attemptm The word that the user writes as input.
     *  @return Points according to the passed attempt.
     */
    @Override
    public int getPointsForWord(String attempt) {
        int point = 0;
        for (String word: valid) {
            if (attempt.equals(word)) {
                if (attempt.length() == 4) {
                    point = 1;
                    pointCount++;
                } else if (attempt.length() == 7) {
                    point = attempt.length() + 7;
                    pointCount += attempt.length() + 7;
                    return point;
                } else if (attempt.length() > 4) {
                    point = attempt.length();
                    pointCount += attempt.length();
                }
            }
        }
        return point;
    }

    /**
     * This message will process a message according to the passed in attempt.
     * For example, if it is right, the message will return "good".
     * @param attempt The word that the user writes as input.
     * @return The message that corresponds to the attempt.
     */
    @Override
    public String getMessage(String attempt) {
        String msg = "not exist";
        for (int i = 0; i < valid.length; i++) {
            if (attempt.equals(valid[i])) {
                msg = "good! (^o^)";
                return msg;
            }
            if (attempt.contentEquals(invalid[i])) {
                msg = "bad (-_-)";
                return msg;
            }
        }
        return msg;
    }

    /**
     * This method will get the 7 letters that was hard coded inside the constructor.
     * @return The hard coded letters.
     */
    @Override
    public String getAllLetters() {
        return letters;
    }

    /**
     * This method returns the center character that is required to be part of every word.
     * @return The center letter of the String letters.
     */
    @Override
    public char getCenterLetter() {
        return center;
    }

    /**
     * This method returns the user's current score.
     * @return the total accumulated score.
     */
    @Override
    public int getScore() {
        return pointCount;
    }

    /**
     * This method will return an array of int hard coded that will represent the thresholds that the user has crossed.
     * @return The array of integers containing the values of thresholds the user crossed.
     */
    @Override
    public int[] getBrackets() {
        int[] threshold = new int[5];
        threshold[0] = 5;
        threshold[1] = 10;
        threshold[2] = 15;
        threshold[3] = 20;
        threshold[4] = 25;

        return threshold;
    }

}